//
//  NavigationBarExtentions.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 4 Av 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//
import UIKit
import Foundation

extension UINavigationController {
    
}

extension UINavigationBar {
    func loadJobimColors() {
        let epicOrangeColor = UIColor.init(red: 0.9, green: 0.27, blue: 0, alpha: 1)
        barTintColor = epicOrangeColor
        tintColor = UIColor.whiteColor()
        titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
    }
}