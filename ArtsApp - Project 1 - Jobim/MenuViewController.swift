//
//  MenuViewController.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 22 Tamuz 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var tabVC:MyTabBarControllerViewController?
    
    var currentVC:String?
    
    @IBOutlet weak var userImageButton: UIButton!
    
    @IBOutlet weak var helloUserLabel: UILabel!
    
    @IBOutlet weak var menuTableView: UITableView!
    
    @IBOutlet weak var rightTableViewConstraint: NSLayoutConstraint!
    
    @IBAction func userImageButtonTapped(sender: AnyObject) {
        if tabVC?.selectedIndex != 0 {
            if Account.sharedAccount.firstName == nil {
                tabVC?.selectedIndex = 0
            } else {
                tabVC?.selectedIndex = 6
            }
            hideMenu()
        } else {
            hideMenu()
        }
    }
    
    let menu = [["My account","Notifications","My jobs"],["Find jobs"],["About us"],["Post a new job"]]
    
    override func viewWillAppear(animated: Bool) {
        let account = Account.sharedAccount
        if account.userImage != nil && account.userImage != "" {
            userImageButton.setImage(UIImage(named:account.userImage!), forState: .Normal)
        }
        if account.firstName != nil && account.firstName != "" {
            helloUserLabel.text = "\(account.firstName!) \(account.lastName!)"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.blackColor()
        return view
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu[section].count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        
        cell.textLabel!.text = menu[indexPath.section][indexPath.row]
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
                //My account
            case 0:
                if tabVC?.selectedIndex != 0 {
                    if Account.sharedAccount.firstName == nil {
                        tabVC?.selectedIndex = 0
                    } else {
                        tabVC?.selectedIndex = 6
                    }
                    hideMenu()
                } else {
                    hideMenu()
                }
                
                //Notifications
            case 1:
                
                if tabVC?.selectedIndex != 1 {
                    tabVC?.selectedIndex = 1
                    hideMenu()
                } else {
                    hideMenu()
                }
                
                //My jobs
            case 2:
                
                if tabVC?.selectedIndex != 2 {
                    tabVC?.selectedIndex = 2
                    hideMenu()
                } else {
                    hideMenu()
                }
                
            default:
                break
            }
            //Find Jobs
        case 1:
            
            if tabVC?.selectedIndex != 3 {
                tabVC?.selectedIndex = 3
                hideMenu()
            } else {
                hideMenu()
            }
            
            //About us
        case 2:
            if tabVC?.selectedIndex != 4 {
                tabVC?.selectedIndex = 4
                hideMenu()
            } else {
                hideMenu()
            }
            
            //Add job
        case 3:
            if tabVC?.selectedIndex != 5 {
                tabVC?.selectedIndex = 5
                hideMenu()
            } else {
                hideMenu()
            }
        default:
            print("aloha")
        }
    }
    
    let dimBlackView = UIView()
    
//    let collectionView : UICollectionView = {
//        let layout = UICollectionViewFlowLayout()
//        let cv = UICollectionView(frame: .zero, collectionViewLayout:  layout)
//        cv.backgroundColor = UIColor.whiteColor()
//        return cv
//    }()
    
    func showMenu() {
        
        if let window = UIApplication.sharedApplication().keyWindow {
            
            
            dimBlackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
            
            dimBlackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideMenu)))
            
            window.addSubview(dimBlackView)
            
            dimBlackView.frame = window.frame
            dimBlackView.alpha = 0
            
            UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .CurveEaseOut, animations: {
                self.dimBlackView.alpha = 1
                }, completion: nil)
            
            //////////////
            
            
            view.frame = CGRect(x: 0, y: 0, width: 0, height: window.frame.height)
            
            view.backgroundColor = UIColor(white: 0, alpha: 0)
            
            self.rightTableViewConstraint.constant = 0 //window.frame.width
            
            self.menuTableView.alpha = 0
            
            userImageButton.alpha = 0
            
            UIView.animateWithDuration(0.2, animations: {
                self.userImageButton.alpha = 1
                
                self.helloUserLabel.alpha = 1
                
                self.view.frame = CGRect(x: 0, y: 0, width: window.frame.width * 0.8, height: window.frame.height)
                
                self.rightTableViewConstraint.constant = window.frame.width * 0.15
                
                self.menuTableView.alpha = 1
                }, completion: { (true) in
                    window.addSubview(self.view)
            })
            
        }
        
    }
    
    func hideMenu() {
        if let window = UIApplication.sharedApplication().keyWindow {
            
            self.userImageButton.alpha = 0
            
            self.helloUserLabel.alpha = 0
            
            self.rightTableViewConstraint.constant = 0
            
            UIView.animateWithDuration(0.5) {
                
                self.dimBlackView.alpha = 0
                
                UIView.animateWithDuration(5, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: 0, height: window.frame.height)
                })
            }
            
        }
    }
    
}
