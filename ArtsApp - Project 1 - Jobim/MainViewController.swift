//
//  MainViewController.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 4 Tamuz 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit
import CoreLocation

class MainViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var jobsTableView: UITableView!
    
    
    
    var jobs:[Job]?
    
    var favoriteJobs:[Job]?
    
    override func viewWillAppear(animated: Bool) {
        
        NSNotificationCenter.defaultCenter().postNotificationName("load", object: nil)
        jobsTableView.reloadData()
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MainViewController.loadList(_:)),name:"load", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MainViewController.removeJob(_:)),name:"remove", object: nil)
        jobsTableView.contentInset = UIEdgeInsetsZero;
        
        
        
        
        
        //        jobs = Jobs.sharedJobs.list
        //        self.jobsTableView.reloadData()
        
    }
    func removeJob(notification:NSNotification) {
        switch self.parentViewController {
            case is MainContainerViewController:
             jobs = Jobs.sharedJobs.list
             case is MyJobsViewController:
                favoriteJobs = []
                for job in Jobs.sharedJobs.list! {
                    if job.favorited {
                        print("HuliFK")
                        favoriteJobs?.append(job)
                    }
                }
                
            jobs = favoriteJobs
        default:
            break
        }
        jobsTableView.reloadData()
    }
    
    func loadList(notification: NSNotification){
        
        switch self.parentViewController {
        case is MainContainerViewController:
            if Account.sharedAccount.userJobProfessionsFilter == nil || Account.sharedAccount.userJobProfessionsFilter! == [Job.Profession.AllJobs]{
                jobs = Jobs.sharedJobs.list
            } else {
                jobs = Jobs.sharedJobs.list
                let filteredJobs = jobs!.filter({job in
                    Account.sharedAccount.userJobProfessionsFilter!.contains(job.profession)
                })
                jobs = filteredJobs
            }
        case is MyJobsViewController:
            
            switch (self.parentViewController as? MyJobsViewController)!.jobsSegment.selectedSegmentIndex {
            case 0:
                jobs = []
            case 1:
                favoriteJobs = []
                for job in Jobs.sharedJobs.list! {
                    if job.favorited {
                        print("HuliFK")
                        favoriteJobs?.append(job)
                    }
                }
                jobs = favoriteJobs
            default:
                jobs = favoriteJobs
            }
            jobsTableView.reloadData()
        default:
            break
        }
        
        //////////////////////////////
        
            jobsTableView.reloadData()
        
        //load data here
        //        jobsTableView.reloadData()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if jobs?.count > 0 {
            return jobs!.count
        } else {
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("cell") as! MainTableViewCell
        
        let job = jobs![indexPath.row]
        
        cell = createJobCell(Job: job, Cell: cell)
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) {
            cell.moveToCenter()
        }
        
        return cell
    }
    
    func createJobCell(Job job: Job,Cell cell: MainTableViewCell) -> MainTableViewCell{
        
        let jobsProfession = job.profession
        
        let jobsCompany = job.company
        
        let jobsTag = job.tag
        
        //Edit center view's top label - branch looking for a profession
        cell.centerView.topLabel.text = "\(jobsCompany) is looking for a \(jobsProfession.rawValue)"
        
        //Edit center view's middle label - description
        cell.centerView.middleLabel.text = "\(jobsTag)"
        
        if job.canEmail == false {
            cell.leftView.emailButton.enabled = false
        }
        
        if job.canCall == false {
            cell.leftView.callButton.enabled = false
        }
        
        if job.canSms == false {
            cell.leftView.smsButton.enabled = false
        }
        if job.favorited {
            cell.leftView.isAddedToFavorites = true
            cell.leftView.favoritesButton.setImage(UIImage(named: "Star Filled"), forState: .Normal)
            cell.leftView.favoritesLabel.text = "Remove from favorites"
            cell.leftView.isAddedToFavorites = true
        } else {
            cell.leftView.favoritesButton.setImage(UIImage(named: "Star Empty"), forState: .Normal)
            cell.leftView.favoritesLabel.text = "Add to favorites"
            cell.leftView.isAddedToFavorites = false
        }
        
        //Switch the image by the job requested profession
        switch jobsProfession {
            
        case Job.Profession.Bartender :
            cell.centerView.jobImage.image = UIImage(named: "WOW - Priest")
        case Job.Profession.BranchManager:
            cell.centerView.jobImage.image = UIImage(named: "WOW - Druid")
        case Job.Profession.Cashier :
            cell.centerView.jobImage.image = UIImage(named: "WOW - Hunter")
        case Job.Profession.Cook :
            cell.centerView.jobImage.image = UIImage(named: "WOW - Rouge")
        case Job.Profession.ConstructionWorker :
            cell.centerView.jobImage.image = UIImage(named: "WOW - Paladin")
        case Job.Profession.Courier :
            cell.centerView.jobImage.image = UIImage(named: "WOW - Shaman")
        case Job.Profession.Driver :
            cell.centerView.jobImage.image = UIImage(named: "WOW - Warlock")
        case Job.Profession.Gardner :
            cell.centerView.jobImage.image = UIImage(named: "WOW - Warlock")
        case Job.Profession.GasStationAttendant :
            cell.centerView.jobImage.image = UIImage(named: "WOW - Warlock")
        case Job.Profession.Host :
            cell.centerView.jobImage.image = UIImage(named: "WOW - Warlock")
        case Job.Profession.Janitor :
            cell.centerView.jobImage.image = UIImage(named: "WOW - Warlock")
        case Job.Profession.KitchenHelper :
            cell.centerView.jobImage.image = UIImage(named: "WOW - Warlock")
        case Job.Profession.SalesAndServiceRepresentative :
            cell.centerView.jobImage.image = UIImage(named: "WOW - Warlock")
        case Job.Profession.SanitationWorker :
            cell.centerView.jobImage.image = UIImage(named: "WOW - Warlock")
        case Job.Profession.SecuriyGuard :
            cell.centerView.jobImage.image = UIImage(named: "WOW - Warlock")
        case Job.Profession.ShiftManager :
            cell.centerView.jobImage.image = UIImage(named: "WOW - Warlock")
        case Job.Profession.StoreSalesman :
            cell.centerView.jobImage.image = UIImage(named: "WOW - Warlock")
        case Job.Profession.Storekeeper :
            cell.centerView.jobImage.image = UIImage(named: "WOW - Warlock")
        case Job.Profession.Teenages :
            cell.centerView.jobImage.image = UIImage(named: "WOW - Warlock")
        case Job.Profession.Usher :
            cell.centerView.jobImage.image = UIImage(named: "WOW - Warlock")
        case Job.Profession.WaiterWaitress :
            cell.centerView.jobImage.image = UIImage(named: "WOW - Warlock")
        case Job.Profession.SellerMan :
            cell.centerView.jobImage.image = UIImage(named: "WOW - Warlock")
        case Job.Profession.Nadlan:
            cell.centerView.jobImage.image = UIImage(named: "WOW - Warlock")
        default:
            break
        }
        return cell
    }
}
