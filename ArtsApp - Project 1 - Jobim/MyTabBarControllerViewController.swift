//
//  myTabBarControllerViewController.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 3 Av 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit
import CoreLocation

class MyTabBarControllerViewController: UITabBarController {
    
    var sideMenuVC:MenuViewController?
    
    override func viewWillAppear(animated: Bool) {
        
        selectedIndex = 3
        if Jobs.sharedJobs.list?.count == 0 || Jobs.sharedJobs.list == nil {
            Jobs.sharedJobs.list = [Job(company: "Delicious INC", branch: nil, profession: Job.Profession.Cook, tag: "Join our family and conquer the city!", info: "Don Godfather will conquer the world", location:CLLocation.init(latitude: 32.076, longitude: 34.8099) , photo: UIImage(),suitableForTeenagers:true, question: nil, canEmail: true, emailAdress: "deliciousinc@hotgmail.com", canCall: false, callNumber: nil, canSms: false, smsNumber: nil),
                                    Job(company: "TipsyCity", branch: "Tipsy on the moon", profession: Job.Profession.Bartender, tag: "If you know how to shake it, you're welcome!", info: nil, location: CLLocation.init(latitude: 32.080, longitude: 34.800), photo: UIImage(),suitableForTeenagers:false, question: nil, canEmail: false, emailAdress: nil, canCall: false, callNumber: nil, canSms: true, smsNumber: "9720000000"),
                                    Job(company: "BestMosheHotels", branch: "RamatGanMosheHotel", profession: Job.Profession.Nadlan, tag: "Would you like to work in the best hotels? Call right now!", info: nil, location: CLLocation.init(latitude: 32.111, longitude: 34.862), photo: UIImage(),suitableForTeenagers:false, question: nil, canEmail: false, emailAdress: nil, canCall: true, callNumber: "12345666777", canSms: false, smsNumber: nil),Job(company: "Delicious INC", branch: nil, profession: Job.Profession.Cook, tag: "Join our family and dominate the city!", info: "Don Godfather will dominate the world", location:CLLocation.init(latitude: 32.076, longitude: 34.8099) , photo: UIImage(),suitableForTeenagers:true, question: nil, canEmail: true, emailAdress: "deliciousinc@hotgmail.com", canCall: false, callNumber: nil, canSms: false, smsNumber: nil),
                                    Job(company: "TipsyCity", branch: "Tipsy on the moon", profession: Job.Profession.Cashier, tag: "If you know how to shake it, you're welcome!", info: nil, location: CLLocation.init(latitude: 32.080, longitude: 34.800), photo: UIImage(),suitableForTeenagers:false, question: nil, canEmail: false, emailAdress: nil, canCall: false, callNumber: nil, canSms: true, smsNumber: "9720000000"),
                                    Job(company: "BestMosheHotels", branch: "RamatGanMosheHotel", profession: Job.Profession.Courier, tag: "Would you like to work in the best hotels? Call right now!", info: nil, location: CLLocation.init(latitude: 32.111, longitude: 34.862), photo: UIImage(),suitableForTeenagers:false, question: nil, canEmail: false, emailAdress: nil, canCall: true, callNumber: "12345666777", canSms: false, smsNumber: nil)]
        }
        if Account.sharedAccount.userNotifications == nil || Account.sharedAccount.userNotifications!.count == 0 {
            Account.sharedAccount.userNotifications = ["asdasdasdasd\nasdasdasdasd","qweqweqweqweqweqweqweqweqweqweqweq qwe qwe qwe qwe qw","123123123zxc 12 31 132 fg  n\nasdasd"]
        }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.hidden = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
