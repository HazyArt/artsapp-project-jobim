//
//  myNameViewController.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 5 Av 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit

class MyNameViewController: UIViewController,UITextFieldDelegate {


    @IBOutlet weak var profileImageButton: UIButton!
    
    @IBOutlet weak var firstNameTextField: UITextField!
    
    @IBOutlet weak var lastNameTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if Account.sharedAccount.firstName != nil {
            firstNameTextField.text = Account.sharedAccount.firstName
            lastNameTextField.text = Account.sharedAccount.lastName
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onPofileImageButtonPress(sender: UIButton) {
        
    }
    
    @IBAction func saveButtonTapped(sender: UIButton) {
        
        if firstNameTextField.text != "" && lastNameTextField.text != "" {
            let account = Account.sharedAccount
            account.firstName = firstNameTextField.text
            account.lastName = lastNameTextField.text
            account.saveAccount()
            self.navigationController?.popViewControllerAnimated(true)
        } else {
            print("EMPTY NAME FIELD")
            //TODO:- ALERT CONTROLLER
        }
        
    }

}
