//
//  CenterViewInMainTableViewCell.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 14 Tamuz 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit

class CenterViewInMainTableViewCell: UIView {

    @IBOutlet weak var topLabel:UILabel!
    
    @IBOutlet weak var jobImage:UIImageView!
    
    @IBOutlet weak var middleLabel:UILabel!
    
    @IBOutlet weak var bottomLabelLocationIconImage:UIImageView!
    
    @IBOutlet weak var rightBottomLabel:UILabel!
    
    @IBOutlet weak var leftBottomLabel:UILabel!
    
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
