//
//  postNewJobViewController.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 4 Av 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit
import CoreLocation

class PostNewJobViewController: UIViewController,UITabBarDelegate {
    
    var companyVC:JobCompanyViewController?
    
    var positionVC:JobPositionViewController?
    
    var tagAndDescriptionVC:JobTagAndDescriptionViewController?
    
    var locationVC:JobLocationViewController?
    
    var contactAndAdditionalInfoVC:JobContactAndAdditionalInfoViewController?
    
    var tabBarVC:MyTabBarControllerViewController?
    
    @IBOutlet weak var rightButton:UIButton!
    
    @IBOutlet weak var container: UIView!
    
    var currentItemTag = 0
    
    var company:String?
    var branch:String?
    var position:Job.Profession?
    var tag:String?
    var jobDescription:String?
    var location:String?
    var asd:String?
    var suitableForTeenagers:Bool?
    
    @IBOutlet weak var tabs: UITabBar!
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBar.loadJobimColors()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabs.selectedItem = tabs.items![0]
        
        rightButton.setTitle("Next", forState: .Normal)
        
        tabBarVC = self.tabBarController as? MyTabBarControllerViewController
        
        tabBarVC!.sideMenuVC = self.storyboard?.instantiateViewControllerWithIdentifier("sideMenuVC") as? MenuViewController
        
        tabBarVC?.sideMenuVC?.tabVC = tabBarVC
        
        companyVC = self.childViewControllers[0] as? JobCompanyViewController
        
        positionVC = self.storyboard?.instantiateViewControllerWithIdentifier("jobPositionVC") as? JobPositionViewController
        
        tagAndDescriptionVC = self.storyboard?.instantiateViewControllerWithIdentifier("jobTagAndDescriptionVC") as? JobTagAndDescriptionViewController
        
        locationVC = self.storyboard?.instantiateViewControllerWithIdentifier("jobLocationVC") as? JobLocationViewController
        
        contactAndAdditionalInfoVC = self.storyboard?.instantiateViewControllerWithIdentifier("jobContactAndAdditionalInfoVC") as? JobContactAndAdditionalInfoViewController
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.Plain, target:nil, action:nil)
        
    }
    
    func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        switch item.tag {
        case 0:
            print("0")
            print("Banana banana banana terracotta banana terracotta terracotta pie!")
            if currentItemTag != 0 && isPreviousTabsInputCorrect(0) {
                //                rightButton.setTitle("Next", forState: .Normal)
                switchContainerViewToCompanyVC()
            }
        case 1:
            print("1")
            if currentItemTag != 1 && isPreviousTabsInputCorrect(item.tag - 1) {
                //                rightButton.setTitle("Next", forState: .Normal)
                switchContainerViewToPositionVC()
            }
        case 2:
            print("2")
            if currentItemTag != 2 && isPreviousTabsInputCorrect(item.tag - 1) {
                //                rightButton.setTitle("Next", forState: .Normal)
                switchContainerViewToTagAndDecriptionVC()
            }
        case 3:
            print("3")
            if currentItemTag != 3 && isPreviousTabsInputCorrect(item.tag - 1) {
                //                rightButton.setTitle("Next", forState: .Normal)
                switchContainerViewToLocationVC()
            }
        case 4:
            print("4")
            if currentItemTag != 4 && isPreviousTabsInputCorrect(item.tag - 1) {
                //                rightButton.setTitle("Next", forState: .Normal)
                switchContainerViewToContactAndAdditionalInfoVC()
            }
        default:
            break
        }
        tabs.selectedItem = tabs.items![currentItemTag]
    }
    
    func isPreviousTabsInputCorrect(itemTag:Int) -> Bool {
        for index in 0...itemTag {
            switch index {
            case 0:
                if self.companyVC?.companyTextField.text == "" {
                    tabs.items![1].enabled = false
                    tabs.items![2].enabled = false
                    tabs.items![3].enabled = false
                    tabs.items![4].enabled = false
                    return false
                }
            case 1:
                if self.positionVC?.jobsTableView.indexPathForSelectedRow == nil {
                    tabs.items![2].enabled = false
                    tabs.items![3].enabled = false
                    tabs.items![4].enabled = false
                    return false
                }
            case 2:
                if self.tagAndDescriptionVC?.tagTextField.text == "" {
                    tabs.items![3].enabled = false
                    tabs.items![4].enabled = false
                    return false
                }
            case 3:
                if self.locationVC?.addressTextField.text == "" {
                    tabs.items![4].enabled = false
                    return false
                }
            case 4:
                continue
            default:
                return false
            }
        }
        return true
    }
    
    
    @IBAction func rightButtonTapped(sender: UIButton) {
        view.endEditing(true)
        if isPreviousTabsInputCorrect(currentItemTag) {
            switch currentItemTag {
            case 0:
                tabs.items![1].enabled = true
                tabs.selectedItem = tabs.items![1]
                company = self.companyVC?.companyTextField.text
                branch = self.companyVC?.branchTextField.text
                switchContainerViewToPositionVC()
            case 1:
                tabs.items![2].enabled = true
                tabs.selectedItem = tabs.items![2]
                position = self.positionVC?.professions[self.positionVC!.jobsTableView.indexPathForSelectedRow!.row]
                switchContainerViewToTagAndDecriptionVC()
            case 2:
                tabs.items![3].enabled = true
                tabs.selectedItem = tabs.items![3]
                tag = self.tagAndDescriptionVC?.tagTextField.text
                jobDescription = self.tagAndDescriptionVC?.descriptionTextField.text
                switchContainerViewToLocationVC()
            case 3:
                location  = self.locationVC?.addressTextField.text
                tabs.items![4].enabled = true
                tabs.selectedItem = tabs.items![4]
                switchContainerViewToContactAndAdditionalInfoVC()
                
            case 4:
                suitableForTeenagers = self.contactAndAdditionalInfoVC?.suitableForTeenagers
                self.performSegueWithIdentifier("finalPostNewJobSegue", sender: self)
            default:
                break
            }
        }
        
    }
    
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction func onMenuButtonPress(sender: UIButton) {
        tabBarVC?.sideMenuVC!.showMenu()
    }
    
    func switchContainerViewToCompanyVC() {
        UIView.transitionWithView(self.view, duration: 0.5, options: .TransitionCrossDissolve , animations: { _ in
            
            self.addChildViewController(self.companyVC!)
            
            self.companyVC!.view.frame = CGRectMake(0, 0, self.container.frame.size.width, self.container.frame.size.height)
            
            self.container.addSubview(self.companyVC!.view)
            
            self.companyVC!.didMoveToParentViewController(self)
            }, completion: nil)
        currentItemTag = 0
    }
    func switchContainerViewToPositionVC() {
        UIView.transitionWithView(self.view, duration: 0.5, options: .TransitionCrossDissolve , animations: { _ in
            
            self.addChildViewController(self.positionVC!)
            
            self.positionVC!.view.frame = CGRectMake(0, 0, self.container.frame.size.width, self.container.frame.size.height)
            
            self.container.addSubview(self.positionVC!.view)
            
            self.positionVC!.didMoveToParentViewController(self)
            }, completion: nil)
        currentItemTag = 1
    }
    
    func switchContainerViewToTagAndDecriptionVC() {
        UIView.transitionWithView(self.view, duration: 0.5, options: .TransitionCrossDissolve , animations: { _ in
            
            self.addChildViewController(self.tagAndDescriptionVC!)
            
            self.tagAndDescriptionVC!.view.frame = CGRectMake(0, 0, self.container.frame.size.width, self.container.frame.size.height)
            
            self.container.addSubview(self.tagAndDescriptionVC!.view)
            
            self.tagAndDescriptionVC!.didMoveToParentViewController(self)
            }, completion: nil)
        currentItemTag = 2
    }
    func switchContainerViewToLocationVC() {
        UIView.transitionWithView(self.view, duration: 0.5, options: .TransitionCrossDissolve , animations: { _ in
            
            self.addChildViewController(self.locationVC!)
            
            self.locationVC!.view.frame = CGRectMake(0, 0, self.container.frame.size.width, self.container.frame.size.height)
            
            self.container.addSubview(self.locationVC!.view)
            
            self.locationVC!.didMoveToParentViewController(self)
            }, completion: nil)
        currentItemTag = 3
    }
    func switchContainerViewToContactAndAdditionalInfoVC() {
        UIView.transitionWithView(self.view, duration: 0.5, options: .TransitionCrossDissolve , animations: { _ in
            
            self.addChildViewController(self.contactAndAdditionalInfoVC!)
            
            self.contactAndAdditionalInfoVC!.view.frame = CGRectMake(0, 0, self.container.frame.size.width, self.container.frame.size.height)
            
            self.container.addSubview(self.contactAndAdditionalInfoVC!.view)
            
            self.contactAndAdditionalInfoVC!.didMoveToParentViewController(self)
            }, completion: nil)
        currentItemTag = 4
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "finalPostNewJobSegue" {
            
            let view = segue.destinationViewController as? finalPostNewJobViewController
            
            view?.company = self.company
            view?.branch = self.branch
            view?.position = self.position
            view?.tag = self.tag
            view?.jobDescription = self.jobDescription
            view?.location = self.location
            view?.suitableForTeenagers = self.suitableForTeenagers
            
        }
    }
}
