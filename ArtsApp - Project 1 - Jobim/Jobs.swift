//
//  Jobs.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 11 Av 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import Foundation

class Jobs: NSObject, NSCoding {
    static var sharedJobs = Jobs()
    var list:[Job]?
    
    override init() {}
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.list, forKey: "jobsList")
    }
    required init?(coder aDecoder: NSCoder) {
        self.list = aDecoder.decodeObjectForKey("jobsList") as? [Job]
    }
    func saveJobs() {
        let savedData = NSKeyedArchiver.archivedDataWithRootObject(Jobs.sharedJobs)
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(savedData, forKey: "Jobs")
    }
    func loadJobs() {
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if let savedJobs = defaults.objectForKey("Jobs") as? NSData {
            Jobs.sharedJobs = (NSKeyedUnarchiver.unarchiveObjectWithData(savedJobs) as! Jobs)
        }
    }
}