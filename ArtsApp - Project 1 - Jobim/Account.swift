//
//  MyAccount.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 6 Av 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import Foundation
import CoreLocation

class Account:NSObject,NSCoding {
    
    static var sharedAccount = Account()
    var firstName:String?
    var lastName:String?
    var city:String?
    var year:Int?
    var email:String?
    var userImage:String?
    var userPhoneNumber:String?
    var userAppliedJobs:[Job]?
    var userFavoriteJobs:[Job]?
    var userJobProfessionsFilterCount:Int?
    var userJobProfessionsFilter:[Job.Profession]?
    var userNotifications:[String]?
    
    override init(){}
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.firstName,forKey: "firstName")
        aCoder.encodeObject(self.lastName,forKey: "lastName")
        aCoder.encodeObject(self.city,forKey: "city")
        aCoder.encodeObject(self.year!, forKey: "year")
        aCoder.encodeObject(self.email,forKey: "email")
        aCoder.encodeObject(self.userImage,forKey: "userImage")
        aCoder.encodeObject(self.userPhoneNumber,forKey: "userPhoneNumber")
        aCoder.encodeObject(self.userFavoriteJobs,forKey: "userFavoriteJobs")
        aCoder.encodeObject(self.userNotifications, forKey: "userNotifications")
        if userJobProfessionsFilter != nil {
            aCoder.encodeInteger(self.userJobProfessionsFilter!.count, forKey: "userJobProfessionsFilterCount")
            for index in 0 ..< userJobProfessionsFilter!.count {
                aCoder.encodeObject(self.userJobProfessionsFilter![index].rawValue)
            }
        }
    }
//    convenience required init?(coder decoder: NSCoder) {
//        self.init()
//        
//        if let medalStrings = decoder.decodeObjectForKey(Key.medals) as? [[String: String]] {
//            medals = medalStrings.map { $0.mapValues { Medal(rawValue: $0) ?? .Unearned } }
//        }
//        
//    }
//    
//    func encodeWithCoder(encoder: NSCoder) {
//        let medalStrings = medals.map { $0.mapValues { $0.rawValue } }
//        encoder.encodeObject(medalStrings, forKey: Key.medals)
//    }
    required init?(coder aDecoder: NSCoder) {
        self.firstName = aDecoder.decodeObjectForKey("firstName") as? String
        self.lastName = aDecoder.decodeObjectForKey("lastName") as? String
        self.city = aDecoder.decodeObjectForKey("city") as? String
        self.year = aDecoder.decodeObjectForKey("year") as? Int
        self.email = aDecoder.decodeObjectForKey("email") as? String
        self.userImage = aDecoder.decodeObjectForKey("userImage") as? String
        self.userPhoneNumber = aDecoder.decodeObjectForKey("userPhoneNumber") as? String
        self.userFavoriteJobs = aDecoder.decodeObjectForKey("userFavoriteJobs") as? [Job]
//        self.userJobProfessionsFilter = aDecoder.decodeObjectForKey("userJobProfessionsFilter") as? [Job.Profession]
        self.userNotifications = aDecoder.decodeObjectForKey("userNotifications") as? [String]
        let prefessionsCounter = aDecoder.decodeIntegerForKey("userJobProfessionsFilterCount")
        self.userJobProfessionsFilter = []
        for index in 0 ..< prefessionsCounter {
            if let filter = aDecoder.decodeObject() as? String {
                self.userJobProfessionsFilter!.append(Job.Profession(rawValue: filter)!)
            }
        }
        
        
        
    }
    func saveAccount() {
        let savedData = NSKeyedArchiver.archivedDataWithRootObject(Account.sharedAccount)
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(savedData, forKey: "Account")
    }
    func loadAccount() {
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if let savedAccount = defaults.objectForKey("Account") as? NSData {
            Account.sharedAccount = (NSKeyedUnarchiver.unarchiveObjectWithData(savedAccount) as! Account)
            Account.sharedAccount.userPhoneNumber = "12345657890"
        }
    }
}