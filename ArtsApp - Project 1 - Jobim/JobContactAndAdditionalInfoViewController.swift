//
//  jobContactAndAdditionalInfoViewController.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 9 Av 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit

class JobContactAndAdditionalInfoViewController: UIViewController {

    @IBOutlet weak var imageButton: UIButton!
    
    @IBOutlet weak var checkBoxButton: UIButton!
    
    @IBOutlet weak var suitableButton: UIButton!
    
    var suitableForTeenagers = false
    
    @IBAction func suitableForTMNTButtonTapped(sender: AnyObject) {
        if suitableForTeenagers {
            checkBoxButton.setImage(UIImage(named: "Unchecked Checkbox"), forState: .Normal)
            suitableButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
        } else {
            checkBoxButton.setImage(UIImage(named: "Checked Checkbox"), forState: .Normal)
            suitableButton.setTitleColor(UIColor.greenColor(), forState: .Normal)
        }
        suitableForTeenagers = !suitableForTeenagers
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
