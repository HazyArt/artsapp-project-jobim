//
//  myJobsViewController.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 4 Av 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit

class MyJobsViewController: UIViewController {
    
    var tabBarVC:MyTabBarControllerViewController?
    
    @IBOutlet weak var jobsSegment: UISegmentedControl!
    
    @IBAction func jobsSegmentedSwitched(sender: UISegmentedControl) {
         NSNotificationCenter.defaultCenter().postNotificationName("load", object: nil)
    }
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBar.loadJobimColors()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarVC = self.tabBarController as? MyTabBarControllerViewController
        tabBarVC!.sideMenuVC = self.storyboard?.instantiateViewControllerWithIdentifier("sideMenuVC") as? MenuViewController
        tabBarVC?.sideMenuVC?.tabVC = tabBarVC
        jobsSegment.selectedSegmentIndex = 1
    }
    
    
    @IBAction func onMenuButtonPress(sender: UIButton) {
        tabBarVC?.sideMenuVC!.showMenu()
    }
    
//    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        switch jobsSegment.selectedSegmentIndex {
//        case 0:
//            if let jobs = Account.sharedAccount.userFavoriteJobs?.count{
//                return jobs
//            }
//        case 1:
//            if let jobs = Account.sharedAccount.userFavoriteJobs?.count{
//                return jobs
//            }
//        default:
//            return 0
//        }
//        return 999
//    }
//    
//    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCellWithIdentifier("cell")
//        switch jobsSegment.selectedSegmentIndex {
//        case 0:
//        case 1:
//        default:
//            break
//        }
//        return cell!
//    }
}
