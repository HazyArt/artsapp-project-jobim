//
//  RightViewInMainTableViewCell.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 14 Tamuz 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit

class RightViewInMainTableViewCell: UIView {

    @IBOutlet weak var deleteButton:UIButton!
    
    @IBOutlet weak var deleteMessageLabel:UILabel!
    
    @IBAction func deleteButtonTapped(sender:UIButton) {
        
        let favorites:[Job] = {
            var result:[Job] = []
            for job in Jobs.sharedJobs.list! {
                if job.favorited {
                    result.append(job)
                }
            }
            return result
        }()
        
        let parent = ((self.superview?.superview?.superview?.superview?.superview?.superview as? UITableView)?.dataSource as? MainViewController)?.parentViewController
        
        let row = (self.superview?.superview?.superview?.superview?.superview?.superview as! UITableView).indexPathForCell(self.superview?.superview?.superview?.superview as! UITableViewCell)?.row
        
        switch parent {
        case is MainContainerViewController:
            for job in Jobs.sharedJobs.list! {
                if job.hashValue == Jobs.sharedJobs.list![row!].hashValue {
                    print("HuliFK")
                    let index = Jobs.sharedJobs.list?.indexOf(job)
                    Jobs.sharedJobs.list!.removeAtIndex(index!)
                    Jobs.sharedJobs.saveJobs()
                    NSNotificationCenter.defaultCenter().postNotificationName("remove", object: nil)
                    break
                }
            }
        case is MyJobsViewController:
            for job in Jobs.sharedJobs.list! {
                if job.hashValue == favorites[row!].hashValue {
                    print("HuliFK")
                    let index = Jobs.sharedJobs.list?.indexOf(job)
                    Jobs.sharedJobs.list!.removeAtIndex(index!)
                    Jobs.sharedJobs.saveJobs()
                    NSNotificationCenter.defaultCenter().postNotificationName("remove", object: nil)
                    break
                }
            }        default:
            break
        }

        
        
        
        
        
    }
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
