//
//  finalPostNewJobViewController.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 10 Av 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit
import CoreLocation

class finalPostNewJobViewController: UIViewController {
    
    var tabVC:MyTabBarControllerViewController?
    
    @IBOutlet weak var smsView: UIView!
    
    @IBOutlet weak var smsCheckboxButton: UIButton!
    
    @IBOutlet weak var smsLabelButton: UIButton!
    
    @IBOutlet weak var smsImage: UIImageView!
    
    var smsNumber = Account.sharedAccount.userPhoneNumber
    
    @IBOutlet weak var callView: UIView!
    
    @IBOutlet weak var callCheckboxButton: UIButton!
    
    @IBOutlet weak var callLabelButton: UIButton!
    
    @IBOutlet weak var callImage: UIImageView!
    
    var callNumber = Account.sharedAccount.userPhoneNumber
    
    @IBOutlet weak var emailView: UIView!
    
    @IBOutlet weak var emailCheckboxButton: UIButton!
    
    @IBOutlet weak var emailLabelButton: UIButton!
    
    @IBOutlet weak var emailImage: UIImageView!
    
    var emailAddress:String?
    
    var smsIsChecked = true
    
    var callIsChecked = true
    
    var emailIsChecked = true
    
    var company:String?
    var branch:String?
    var position:Job.Profession?
    var tag:String?
    var jobDescription:String?
    var location:String?
    var asd:String?
    var suitableForTeenagers:Bool?
    
    @IBOutlet weak var instructionLabel: UILabel!
    
    @IBOutlet weak var il972Label: UILabel!
    
    @IBOutlet weak var il972WidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var il972BottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var inputTextField: UITextField!
    
    @IBOutlet weak var inputFieldBottomConstraint: NSLayoutConstraint!
    
    @IBAction func topCheckOrLabelButtonTapped(sender: AnyObject) {
        if smsIsChecked{
            smsCheckboxButton.setImage(UIImage(named: "Unchecked Checkbox"), forState: .Normal)
            smsLabelButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
            smsImage.alpha = 0.1
            smsView.backgroundColor = UIColor.whiteColor()
        } else {
            smsCheckboxButton.setImage(UIImage(named: "Checked Checkbox"), forState: .Normal)
            smsLabelButton.setTitleColor(UIColor.orangeColor(), forState: .Normal)
            smsImage.alpha = 1
            smsView.backgroundColor = UIColor.grayColor()
            setSmsViewSettings()
            
        }
        
        smsIsChecked = !smsIsChecked
    }
    
    @IBAction func middleCheckOrLabelButtonTapped(sender: AnyObject) {
        if callIsChecked{
            callCheckboxButton.setImage(UIImage(named: "Unchecked Checkbox"), forState: .Normal)
            callLabelButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
            callImage.alpha = 0.1
            callView.backgroundColor = UIColor.whiteColor()
        } else {
            callCheckboxButton.setImage(UIImage(named: "Checked Checkbox"), forState: .Normal)
            callLabelButton.setTitleColor(UIColor.orangeColor(), forState: .Normal)
            callImage.alpha = 1
            callView.backgroundColor = UIColor.grayColor()
            setCallViewSettings()
            
        }
        callIsChecked = !callIsChecked
    }
    
    @IBAction func bottomCheckOrLabelButtonTapped(sender: AnyObject) {
        if emailIsChecked {
            emailCheckboxButton.setImage(UIImage(named: "Unchecked Checkbox"), forState: .Normal)
            emailLabelButton.setTitleColor(UIColor.grayColor(), forState: .Normal)
            emailImage.alpha = 0.1
            emailView.backgroundColor = UIColor.whiteColor()
        } else {
            emailCheckboxButton.setImage(UIImage(named: "Checked Checkbox"), forState: .Normal)
            emailLabelButton.setTitleColor(UIColor.orangeColor(), forState: .Normal)
            emailImage.alpha = 1
            emailView.backgroundColor = UIColor.grayColor()
            setEmailViewSettings()
        }
        emailIsChecked = !emailIsChecked
    }
    
    var selector = 0
    
    @IBAction func smsViewTapped(sender: UITapGestureRecognizer) {
        setSmsViewSettings()
    }
    
    @IBAction func callViewTapped(sender: UITapGestureRecognizer) {
        setCallViewSettings()
        
    }
    
    @IBAction func emailViewTapped(sender: AnyObject) {
        setEmailViewSettings()
        
    }
    func setSmsViewSettings() {
        inputTextField.resignFirstResponder()
        instructionLabel.text = "SMS will be sent to:"
        inputTextField.keyboardType = UIKeyboardType.PhonePad
        inputTextField.becomeFirstResponder()
        il972WidthConstraint.constant = 70
        inputTextField.text = smsNumber
        selector = 0
    }
    
    func setCallViewSettings() {
        inputTextField.resignFirstResponder()
        instructionLabel.text = "The call will be directed to phone number:"
        inputTextField.keyboardType = UIKeyboardType.PhonePad
        inputTextField.becomeFirstResponder()
        il972WidthConstraint.constant = 70
        inputTextField.text = callNumber
        selector = 1
    }
    
    func setEmailViewSettings() {
        inputTextField.resignFirstResponder()
        instructionLabel.text = "E-mails will be sent to:"
        inputTextField.keyboardType = UIKeyboardType.EmailAddress
        inputTextField.becomeFirstResponder()
        il972WidthConstraint.constant = 0
        inputTextField.text = emailAddress
        selector = 2
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        inputTextField.text = smsNumber
        
        smsView.backgroundColor = UIColor.grayColor()
        callView.backgroundColor = UIColor.grayColor()
        emailView.backgroundColor = UIColor.grayColor()
        
        il972Label.layer.borderWidth = 1
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
        
        tabVC = self.storyboard?.instantiateViewControllerWithIdentifier("tabVC") as? MyTabBarControllerViewController
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func keyboardWillShow(notification: NSNotification) {
        switch selector {
        case 0:
            inputTextField.text = smsNumber
        case 1:
            inputTextField.text = callNumber
        case 2:
            inputTextField.text = emailAddress
        default:
            break
        }
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            inputFieldBottomConstraint.constant = keyboardSize.height
            il972BottomConstraint.constant = keyboardSize.height
            UIView.animateWithDuration(0) {
                self.view.layoutIfNeeded()
            }
        }
    }
    func keyboardWillHide(notifiation: NSNotification) {
        saveLines()
        
    }
    func saveLines (){
        switch selector {
        case 0:
            smsNumber = inputTextField.text
        case 1:
            callNumber = inputTextField.text
        case 2:
            emailAddress = inputTextField.text
        default:
            break
        }
    }
    
    @IBAction func postButtonTapped(sender: UIButton) {
        saveLines()
        var isInputCorrect = true
        if smsIsChecked || callIsChecked || emailIsChecked {
            if smsIsChecked && (smsNumber == "" || smsNumber == nil ){
                isInputCorrect = false
            }
            if callIsChecked && (callNumber == "" || callNumber == nil ){
                isInputCorrect = false
            }
            if emailIsChecked && (emailAddress == "" || emailAddress == nil) {
                isInputCorrect = false
            }
        } else {
            isInputCorrect = false
        }
        //32.086
        //34.802
        if isInputCorrect {
            let newJob = Job(company: company!, branch: branch, profession: position!, tag: tag!, info: jobDescription, location:CLLocation.init(latitude: 32.086, longitude: 34.802), photo: nil, suitableForTeenagers: suitableForTeenagers!, question: nil, canEmail: emailIsChecked, emailAdress: emailAddress, canCall: callIsChecked, callNumber: callNumber, canSms: smsIsChecked, smsNumber: smsNumber)
            Jobs.sharedJobs.list?.append(newJob)
            Jobs.sharedJobs.saveJobs()
            self.presentViewController(tabVC!, animated: true, completion: {
                self.tabVC?.selectedIndex = 3
            })
        } else {
            print("incorrect input")
        }
        
    }
}
