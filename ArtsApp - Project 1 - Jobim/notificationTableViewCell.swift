//
//  notificationTableViewCell.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 14 Av 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit

class notificationTableViewCell: UITableViewCell {

    @IBOutlet weak var notificationTextView: UITextView!
    
    @IBOutlet weak var dateAndTimeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
