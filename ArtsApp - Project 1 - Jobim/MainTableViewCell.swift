//
//  MainTableViewCell.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 7 Tamuz 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//



import UIKit

class MainTableViewCell: UITableViewCell, UIScrollViewDelegate {
    
    var cellVC:CellViewController?
    
    var oldScrollViewOffset = CGFloat()
    
    var currentScrollViewOffset = CGFloat()
    
    let screenWidth = UIScreen.mainScreen().bounds.width
    
    
    // MARK:- View Outlet
    
    @IBOutlet weak var jobsScrollView: UIScrollView!
    
    @IBOutlet weak var contentViewOutlet: UIView!
    
    @IBOutlet weak var leftView: LeftViewInMainTableViewCell!
    
    @IBOutlet weak var centerView: CenterViewInMainTableViewCell!
    
    @IBOutlet weak var rightView: RightViewInMainTableViewCell!
    
    @IBOutlet weak var generalView: UIView!
    
    //Mark:-
    //Mark:Left view button actions
    
    
    
    //MARK:- Constraints
    @IBOutlet weak var centerViewWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var generalViewWidthConstraint: NSLayoutConstraint!
    
    //MARK:-
    //MARK:Initialize
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        centerView.topLabel.text = "Kakuna Ratata"
        
        centerView.middleLabel.text = "What a wonderful phrase"
        
        //Adjust the general view size to the scroll view
        //        generalViewWidthConstraint.constant = contentViewOutlet.frame.width + (contentViewOutlet.frame.width*0.95*2)
        
        generalViewWidthConstraint.constant = screenWidth*3
        
        centerViewWidthConstraint.constant = screenWidth
        
        oldScrollViewOffset = jobsScrollView.contentOffset.x
        currentScrollViewOffset = jobsScrollView.contentOffset.x
        
        //Single tap
        let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainTableViewCell.singleTapped(_:)))
        singleTapGestureRecognizer.delegate = self
        singleTapGestureRecognizer.numberOfTapsRequired = 1
        singleTapGestureRecognizer.enabled = true
        singleTapGestureRecognizer.cancelsTouchesInView = false
//        centerView.addGestureRecognizer(singleTapGestureRecognizer)
//        rightView.addGestureRecognizer(singleTapGestureRecognizer)
//        leftView.addGestureRecognizer(singleTapGestureRecognizer)
        generalView.addGestureRecognizer(singleTapGestureRecognizer)
        
    }
    override func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        if touch.view == self.leftView.favoritesButton || touch.view == self.leftView.smsButton || touch.view == self.leftView.callButton || touch.view == self.leftView.emailButton || touch.view == self.rightView.deleteButton {
            return false
        }
        return true
    }
    
    //MARK:- Functions
    func singleTapped(sender: UITapGestureRecognizer) {
        
        print("halloa halloa")
        let parent = ((self.superview?.superview as! UITableView).dataSource as? MainViewController)?.parentViewController
        cellVC = parent?.storyboard?.instantiateViewControllerWithIdentifier("cellVC") as? CellViewController
        parent?.presentViewController(cellVC!, animated: true, completion: nil)
    }
    
    func moveToCenter(){
        jobsScrollView.contentOffset.x = screenWidth
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        oldScrollViewOffset = jobsScrollView.contentOffset.x
    }
    var screenNumber = 1
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let leftScreenOffset = CGFloat(0)
        let centerScreenOffset = screenWidth
        let rightScreenOffset = screenWidth*2
        
        let screens = [leftScreenOffset,centerScreenOffset,rightScreenOffset]
        currentScrollViewOffset = jobsScrollView.contentOffset.x
        //
        //        if currentScrollViewOffset > oldScrollViewOffset + 200  {
        //            print("scroller right")
        //            screenNumber += (screenNumber == 2 ? 0 : 1)
        //            UIView .animateWithDuration(0.05, animations: {
        //                self.jobsScrollView.contentOffset.x = screens[self.screenNumber]
        //            })
        //
        //        } else if currentScrollViewOffset < oldScrollViewOffset - 200 {
        //            print("scrolled left")
        //            screenNumber -= (screenNumber == 0 ? 0 : 1)
        //            UIView .animateWithDuration(0.05, animations: {
        //                self.jobsScrollView.contentOffset.x = screens[self.screenNumber]
        //            })
        //        } else {
        //            UIView .animateWithDuration(0.05, animations: {
        //                self.jobsScrollView.contentOffset.x = self.oldScrollViewOffset
        //
        //            })
        //        }
    }
    
}
