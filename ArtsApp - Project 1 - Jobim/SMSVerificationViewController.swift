//
//  SMSVerificationViewController.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 5 Tamuz 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit

class SMSVerificationViewController: UIViewController,UITextFieldDelegate {

    var generatedCode = "1234"
    
    @IBOutlet weak var numberLabel: UILabel!
    
    @IBOutlet weak var verificationCodeTextField: UITextField!
    @IBOutlet weak var verificationCodeBottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let number = "1234525"
        numberLabel.text = "For No:\(number)"

        // Do any additional setup after loading the view.
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)

        verificationCodeTextField.delegate = self
        verificationCodeTextField.keyboardType = UIKeyboardType.NumberPad
        verificationCodeTextField.tintColor = UIColor.clearColor()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.verificationCodeTextField.becomeFirstResponder()
    }

    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            //            self.view.frame.origin.y -= keyboardSize.height
             verificationCodeBottomConstraint.constant = keyboardSize.height
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //Range of textFieldRange
    var nsRange = NSRange(location: 0, length: 1)

    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let nsString = textField.text! as NSString
        if string == "" {
            nsRange.location += (nsRange.location == 0 ? 0 : -5)
            textField.text! = nsString.stringByReplacingCharactersInRange(nsRange,
                                                                          withString: "-")
        } else {
            let maxLength = 15
            
            
            
            if nsRange.location <= maxLength {
                textField.text! = nsString.stringByReplacingCharactersInRange(nsRange,
                                                                              withString: string)
                nsRange.location += 5
                
                return textField.text?.characters.count <= maxLength
            } else {
                return false
            }
        }
        return false
    }
    
    func validateCode(string:String) -> Bool{
        //let code = string.tri
        return false
    }
    @IBAction func nextButtonTapped(sender: AnyObject) {
        let code = verificationCodeTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()).stringByReplacingOccurrencesOfString(" ", withString: "")
        
        if code != "" && code == generatedCode {
            //let view = self.storyboard?.instantiateViewControllerWithIdentifier("tabVC") as? MyTabBarControllerViewController
            //presentViewController(view!, animated: true, completion: nil)
            //performSegueWithIdentifier("confirmationCompleteSegue", sender: self)
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let initViewController: MyTabBarControllerViewController? = storyBoard.instantiateViewControllerWithIdentifier("tabVC") as? MyTabBarControllerViewController
            UIApplication.sharedApplication().keyWindow!.rootViewController? = initViewController!
        } else {
            print("incorrect code")
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
