//
//  MyEMailViewController.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 5 Av 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit

class MyEMailViewController: UIViewController {

    @IBOutlet weak var myEMailTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if Account.sharedAccount.email != nil {
            myEMailTextField.text = Account.sharedAccount.email
        }
    }
    @IBAction func saveButtonTapped(sender: UIButton) {
        let account = Account.sharedAccount
        account.email = myEMailTextField.text
        account.saveAccount()
        self.navigationController?.popViewControllerAnimated(true)
    }
}
