//
//  myCityViewController.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 5 Av 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit

class MyCityViewController: UIViewController {

    @IBOutlet weak var myCityTextField: UITextField!
    
    @IBOutlet weak var citiesTableView: UITableView!
    
    var pickedCity = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if Account.sharedAccount.city != nil {
            myCityTextField.text = Account.sharedAccount.city
        }
    }
    @IBAction func saveButtonTapped(sender:UIButton) {
        if myCityTextField.text != "" && myCityTextField.text != nil && pickedCity{
            let account = Account.sharedAccount
            account.city = myCityTextField.text
            account.saveAccount()
            self.navigationController?.popViewControllerAnimated(true)
        }
    }

}
