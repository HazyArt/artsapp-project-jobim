//
//  jobTagAndDescriptionViewController.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 9 Av 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit

class JobTagAndDescriptionViewController: UIViewController {

    @IBOutlet weak var jobLabel: UILabel!
    
    @IBOutlet weak var tagTextField: UITextField!
    
    @IBOutlet weak var descriptionTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    
}
