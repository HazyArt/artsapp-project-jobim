//
//  MainContainerViewController.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 20 Tamuz 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
///

import UIKit
import CoreLocation

class MainContainerViewController: UIViewController,CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    
    
    var onMenuPage = true
    
    var filterVC:FilterViewController?
    
    var tabBarVC:MyTabBarControllerViewController?
    
    @IBOutlet weak var topRightBarButton: UIButton!
    
    @IBOutlet weak var jobSearchOptionsButton: UIButton!
    
    @IBOutlet weak var container: UIView!
    
    @IBOutlet weak var filterButton: UIButton!
    
    @IBAction func filterButtonTapped(sender: AnyObject) {
        filterVC?.showFilterMenu()
    }
    
    var mapView:MapViewController?
    
    func openCell() {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("cellVC") as! CellViewController
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    let epicOrangeColor = UIColor.init(red: 0.9, green: 0.27, blue: 0, alpha: 1)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView = self.storyboard?.instantiateViewControllerWithIdentifier("mapViewControllerID") as? MapViewController
        
        
        tabBarVC = self.tabBarController as? MyTabBarControllerViewController
        tabBarVC!.sideMenuVC = self.storyboard?.instantiateViewControllerWithIdentifier("sideMenuVC") as? MenuViewController
        tabBarVC?.sideMenuVC?.tabVC = tabBarVC
        
        //Jobs search option button
        jobSearchOptionsButton.layer.borderWidth = 1
        jobSearchOptionsButton.layer.borderColor = epicOrangeColor.CGColor
        
        filterVC = self.storyboard?.instantiateViewControllerWithIdentifier("filterVC") as? FilterViewController
        filterVC?.mainContainer = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
    }
    override func viewWillAppear(animated: Bool) {
        //MARK:- Navigation Brt Edit
        self.navigationController!.navigationBar.loadJobimColors()
        
        locationManager.startUpdatingLocation()
    }
    
    @IBAction func menuButtonPressed(sender: UIButton) {
        tabBarVC?.sideMenuVC!.showMenu()
    }
    
    
    
    @IBAction func jobsTabeAndMapSwitchButton(sender: UIButton) {
        switchContainerView(onMenuPage)
        onMenuPage = (onMenuPage == true ? false : true)
        
    }
    
    
    func switchContainerView(onMenuPage:Bool){
        
        if onMenuPage {
            
            topRightBarButton.setImage(UIImage(named: "VirtualMachineWindows.png"), forState: .Normal)
            
            UIView.transitionWithView(self.view, duration: 0.5, options: [.TransitionCurlUp], animations: { _ in
                
                self.addChildViewController(self.mapView!)
                
                self.mapView!.view.frame = CGRectMake(0, 0, self.container.frame.size.width, self.container.frame.size.height)
                
                self.container.addSubview(self.mapView!.view)
                
                self.mapView!.didMoveToParentViewController(self)
                }, completion: nil)
            
        } else {
            
            topRightBarButton.setImage(UIImage(named: "LocationIcon.ong"), forState: .Normal)
            
            UIView.transitionWithView(self.view, duration: 0.5, options: .TransitionFlipFromRight , animations: { _ in
                
                // Notify Child View Controller
                self.mapView!.willMoveToParentViewController(nil)
                
                // Remove Child View From Superview
                self.mapView!.view.removeFromSuperview()
                
                // Notify Child View Controller
                self.mapView!.removeFromParentViewController()
                }, completion: nil)
            
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("LOCATIONNNN")
        //get last coordinate
        let lastCoordinate = locations[locations.count - 1]
        
        //get latitude and longtitude
        let lat = lastCoordinate.coordinate.latitude
        let lon = lastCoordinate.coordinate.longitude
        let myCoordinate2D = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        
        //set span
        let lanDelta = 0.05
        let lonDelta = 0.05
        
        //center map
//        mapView?.mapView.setRegion(myCurrentLocation, animated: true)
        
        //add annotation
//        let myAnnotation = MKPointAnnotation()
//        myAnnotation.coordinate = myCoordinate2D
//        mapView?.mapView.addAnnotation(myAnnotation)
        
        
        
        
        
        self.locationManager.stopUpdatingLocation()
    }
    
}
