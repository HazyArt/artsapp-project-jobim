//
//  RegulationsViewController.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 4 Tamuz 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit

class RegulationsViewController: UIViewController {

    var path = ""
    @IBOutlet weak var regulationsSegmentControl: UISegmentedControl!
    
    @IBOutlet weak var regulationsWebView: UIWebView!
    
    @IBAction func onRegulationsSegmentChange(sender: UISegmentedControl) {
        
        switch(regulationsSegmentControl.selectedSegmentIndex){
        case 0:
            path = NSBundle.mainBundle().pathForResource("Terms of use", ofType: "pdf")!
        case 1:
            path = NSBundle.mainBundle().pathForResource("Privacy policy", ofType: "pdf")!
        default:
            break
        }
        
        let url = NSURL.fileURLWithPath(path)
        self.regulationsWebView.loadRequest(NSURLRequest(URL: url))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        path = NSBundle.mainBundle().pathForResource("Terms of use", ofType: "pdf")!
        let url = NSURL.fileURLWithPath(path)
        self.regulationsWebView.loadRequest(NSURLRequest(URL: url))
        
        let attr = NSDictionary(object: UIFont(name: "HelveticaNeue-Bold", size: 15.0)!, forKey: NSFontAttributeName)
        regulationsSegmentControl.setTitleTextAttributes(attr as [NSObject : AnyObject] , forState: .Selected)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
