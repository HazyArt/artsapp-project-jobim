//
//  myBirthdayYearViewController.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 5 Av 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit

class MyBirthdayYearViewController: UIViewController,UIPickerViewDelegate {
    
    @IBOutlet weak var yearPickerView: UIPickerView!
    
    var formatter:NSDateFormatter?
    
    var currentYear:Int?
    
    var pickerYears:[Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        formatter = NSDateFormatter()
        formatter!.setLocalizedDateFormatFromTemplate("yyyy")
        let date = NSDate()
        currentYear = Int(formatter!.stringFromDate(date))
        for year in (currentYear!-100...currentYear!).reverse() {
            pickerYears.append(year)
        }
        if Account.sharedAccount.year == nil {
            yearPickerView.selectRow(16, inComponent: 0, animated: false)
        } else {
            let yearIndex = pickerYears.indexOf(Account.sharedAccount.year!)
            yearPickerView.selectRow(yearIndex!, inComponent: 0, animated: false)
        }
        
        // Do any additional setup after loading the view.
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerYears.count
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(pickerYears[row])
    }
    
    @IBAction func saveButtonTapped(sender:UIButton) {
        if yearPickerView.selectedRowInComponent(0) >=  16 {
            let account = Account.sharedAccount
            account.year = pickerYears[yearPickerView.selectedRowInComponent(0)]
            account.saveAccount()
            self.navigationController?.popViewControllerAnimated(true)
        } else {
            print("TOO YOUNG!")
        }
    }
    
}
