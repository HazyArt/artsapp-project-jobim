//
//  FilterViewController.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 12 Av 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit
import CoreLocation

class FilterViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UISearchDisplayDelegate {
    
    
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBAction func cancelButtonTapped(sender: UIButton) {
        hideFilterMenu()
    }
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var confirmButton: UIButton!
    
    var mainContainer : MainContainerViewController?
    
    var filteredJobs:[Job] = []
    
    //    var selectedProfessions:[Job.Profession] = [Job.Profession.AllJobs]
    var selectedProfessions:[Job.Profession] = [Job.Profession.Bartender,Job.Profession.Cook]
    
    @IBAction func confirmButtonTapped(sender: UIButton) {
        
        //        let jobs = Jobs.sharedJobs.list
        
        //        filteredJobs = jobs?.filter{job in
        //            return job.profession.rawValue.lowercaseString.containsString(<#T##other: String##String#>)
        //        }
        
        if selectedProfessions != [Job.Profession.AllJobs] {
            
            let jobs = Jobs.sharedJobs.list
            
            Account.sharedAccount.userJobProfessionsFilter = selectedProfessions
            Account.sharedAccount.saveAccount()
            NSNotificationCenter.defaultCenter().postNotificationName("load", object: nil)
            //TODO:- Closure()
        } else {
            Account.sharedAccount.userJobProfessionsFilter = []
        }
        NSNotificationCenter.defaultCenter().postNotificationName("load", object: nil)
        hideFilterMenu()
        
        
    }
    
    @IBOutlet weak var filterMenuSegmentedController: UISegmentedControl!
    
    @IBAction func filterMenuSegmentTapped(sender: UISegmentedControl) {
        switch filterMenuSegmentedController.selectedSegmentIndex {
        case 0:
            self.searchTableView.allowsMultipleSelection = false
        case 1:
            self.searchTableView.allowsMultipleSelection = false
        case 2:
            self.searchTableView.allowsMultipleSelection = true
        default:
            break
        }
    }
    
    
    @IBAction func filterMenuTapped(sender: UISegmentedControl) {
        searchTableView.reloadData()
    }
    
    @IBOutlet weak var searchTableView: UITableView!
    
    var dimBlackView = UIView()
    
    var companies:[String]?
    var locations:[String]?
    var allProfessions = Job.allJobs
    
    var filteredCompanies:[String]?
    var filteredLocations:[String]?
    var filteredProfessions:[Job.Profession]?
    
    //    func filterCompany(searchText:String, scope : String = "All") {
    //        switch filterMenuSegmentedController.selectedSegmentIndex {
    //        case 0:
    //            filteredCompanies = companies!.filter { company in
    //                return company.lowercaseString.containsString(searchText.lowercaseString)
    //            }
    //        case 1:
    //            filteredLocatiuons = locations!.filter { location in
    //                return location.lowercaseString.containsString(searchText.lowercaseString)
    //            }
    //
    //        case 2:
    //            filteredJobs = jobs!.filter { job in
    //                return job.rawValue.lowercaseString.containsString(searchText.lowercaseString)
    //            }
    //
    //        default:
    //            break
    //        }
    //        searchTableView.reloadData()
    //    }
    //
    func filterContentForSearchText(searchText: String) {
        switch filterMenuSegmentedController.selectedSegmentIndex {
        case 0:
            // Filter the array using the filter method
            if self.companies == nil {
                self.filteredCompanies = nil
                return
            }
            self.filteredCompanies = self.companies!.filter({( company: String) -> Bool in
                // to start, let's just search by name
                return company.lowercaseString.rangeOfString(searchText.lowercaseString) != nil
            })
        case 1:
            if self.locations == nil {
                self.filteredCompanies = nil
                return
            }
            self.filteredLocations = self.locations!.filter({ (location:String) -> Bool in
                return location.lowercaseString.rangeOfString(searchText.lowercaseString) != nil
            })
        case 2:
            if selectedProfessions == [] {
                self.filteredProfessions = nil
                return
            }
            self.filteredProfessions = self.allProfessions.filter({ (profession:Job.Profession) -> Bool in
                return profession.rawValue.lowercaseString.rangeOfString(searchText.lowercaseString) != nil
            })
        default:
            break
        }
        
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        switch filterMenuSegmentedController.selectedSegmentIndex {
        case 0:
            if searchText != ""{
                filterContentForSearchText(searchText)
                //                tableView(searchTableView, numberOfRowsInSection: filteredCompanies!.count)
                //            } else {
                //                tableView(searchTableView, numberOfRowsInSection: companies!.count)
            }
        case 1:
            if searchText != ""{
                filterContentForSearchText(searchText)
                //                tableView(searchTableView, numberOfRowsInSection: filteredLocations!.count)
                //            } else {
                //                tableView(searchTableView, numberOfRowsInSection: locations!.count)
            }
        case 2:
            if searchText != ""{
                filterContentForSearchText(searchText)
                //                tableView(searchTableView, numberOfRowsInSection: filteredJobs!.count)
                //searchTableView.numberOfRowsInSection(filteredJobs!.count)
                //            } else {
                //                tableView(searchTableView, numberOfRowsInSection: jobs!.count)
            }
        default:
            break
        }
        //searchTableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        companies = ["HOT","BEZEQ","Celcome","Starbucks","Paz"]
        locations = ["111","222","333"]
        
        selectedProfessions = Account.sharedAccount.userJobProfessionsFilter!
        self.searchTableView.allowsMultipleSelection = true
        filterMenuSegmentedController.selectedSegmentIndex = 2
        
        
    }
    
    func showFilterMenu() {
        if let window = UIApplication.sharedApplication().keyWindow {
            
            //let navigationHeight = window.rootViewController?.navigationController?.navigationBar.frame.maxY
            
            if let navigationHeight = window.rootViewController?.childViewControllers[3].childViewControllers[0].navigationController?.navigationBar.frame.maxY {
                
                view.frame = CGRect(x: 0, y: navigationHeight, width: window.frame.width, height: 0)
                
                dimBlackView.frame = window.frame
                
                dimBlackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
                
                dimBlackView.alpha = 0
                
                dimBlackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideFilterMenu)))
                
                window.addSubview(dimBlackView)
                
                UIView.animateWithDuration(0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .CurveEaseOut, animations: {
                    
                    self.dimBlackView.alpha = 1
                    
                    }, completion: nil)
                
                UIView.animateWithDuration(0.1, animations: {
                    self.view.frame = CGRect(x: 0, y:  navigationHeight, width: window.frame.width, height: window.frame.height - navigationHeight)
                    self.cancelButton.alpha = 1
                    self.searchBar.alpha = 1
                    self.titleLabel.alpha = 1
                    self.confirmButton.alpha = 1
                    self.filterMenuSegmentedController.alpha = 1
                    self.searchTableView.alpha = 1
                    }, completion: { (true) in
                        window.addSubview(self.view)
                })
                
                //            view.backgroundColor = UIColor(white: 0, alpha: 0)
                
                
            }
        }
    }
    
    func hideFilterMenu() {
        
        if searchBar.isFirstResponder() {
            searchBar.resignFirstResponder()
        }
        
        if let window = UIApplication.sharedApplication().keyWindow {
            
            let navigationHeight = window.rootViewController!.childViewControllers[3].childViewControllers[0].navigationController!.navigationBar.frame.maxY
            
            cancelButton.alpha = 0
            searchBar.alpha = 0
            titleLabel.alpha = 0
            confirmButton.alpha = 0
            filterMenuSegmentedController.alpha = 0
            searchTableView.alpha = 0
            
            UIView.animateWithDuration(0.3) {
                
                self.dimBlackView.alpha = 0
                
                UIView.animateWithDuration(2, animations: {
                    self.view.frame = CGRect(x: 0, y: navigationHeight, width: window.frame.width, height: 0)
                })
                
            }
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch filterMenuSegmentedController.selectedSegmentIndex {
        case 0:
            break
        case 1:
            break
        case 2:
            if selectedProfessions == [Job.Profession.AllJobs] {
                selectedProfessions = [allProfessions[indexPath.row]]
            } else {
                if selectedProfessions.contains(allProfessions[indexPath.row]){
                    let index = selectedProfessions.indexOf(allProfessions[indexPath.row])
                    selectedProfessions.removeAtIndex(index!)
                    
                    
                } else {
                    selectedProfessions.append(allProfessions[indexPath.row])
                }
            }
        default:
            break
        }
        if selectedProfessions == [] {
            selectedProfessions = [Job.Profession.AllJobs]
        }
        searchTableView.reloadData()
        Account.sharedAccount.userJobProfessionsFilter = selectedProfessions
        Account.sharedAccount.saveAccount()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch filterMenuSegmentedController.selectedSegmentIndex {
        //Companies
        case 0:
            
            if filteredCompanies != nil && filteredCompanies?.count != 0 {
                return filteredCompanies!.count
            } else if filteredCompanies?.count != 0 {
                return companies!.count
            } else {
                return 0
            }
        //Locations
        case 1:
            if filteredLocations != nil && filteredLocations?.count != 0 {
                return filteredLocations!.count
            } else if filteredLocations?.count != 0 {
                return locations!.count
            } else {
                return 0
            }
        //Jobs
        case 2:
            //            if jobs != nil {
            //                return jobs!.count
            //            }
            if filteredProfessions != nil{// && filteredJobs?.count != 0 {
                return filteredProfessions!.count
            } else if filteredProfessions?.count != 0 {
                return allProfessions.count
            } else {
                return 0
            }
        default:
            return 0
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        switch filterMenuSegmentedController.selectedSegmentIndex {
        //Companies
        case 0:
            if filteredCompanies != nil && filteredCompanies?.count != 0 {
                cell.textLabel!.text = filteredCompanies![indexPath.row]
            } else {
                cell.textLabel!.text = companies?[indexPath.row]
            }
        //Locations
        case 1:
            if filteredLocations != nil && filteredLocations!.count != 0 {
                cell.textLabel!.text = filteredLocations![indexPath.row]
            } else {
                cell.textLabel!.text = locations?[indexPath.row]
            }
        //Jobs
        case 2:
            cell.textLabel!.text = allProfessions[indexPath.row].rawValue
            
            let selectedProfessions = Account.sharedAccount.userJobProfessionsFilter
            
                if selectedProfessions!.contains(allProfessions[indexPath.row]) {
                    cell.selected = true
                    cell.backgroundColor = UIColor.cyanColor()
                } else {
                    cell.selected = false
                    cell.backgroundColor = UIColor.whiteColor()
                }
            
        default:
            cell.textLabel!.text = "EMPTY"
        }
        
        return cell
    }
    
}
