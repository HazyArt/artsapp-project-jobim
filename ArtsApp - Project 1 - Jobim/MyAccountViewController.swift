//
//  myAccountViewController.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 28 Tamuz 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit

class myAccountViewController: UIViewController,UITabBarDelegate {
    
    
    @IBOutlet weak var fullnameTab: UITabBar!
    
    @IBOutlet weak var locationTab: UITabBar!
    
    @IBOutlet weak var birthdayTab: UITabBar!
    
    @IBOutlet weak var emailTab: UITabBar!
    
    @IBOutlet weak var container: UIView!
    
    @IBOutlet weak var rightNavigationButton: UIButton!
    
    var tabBarVC:MyTabBarControllerViewController?
    
    var myNameVC:MyNameViewController?
    
    var firstName:String?
    
    var lastName:String?
    
    var city:String?
    
    var year:Int?
    
    var email:String?
    
    var myCityVC:MyCityViewController?
    
    var myBirthdayYearVC:MyBirthdayYearViewController?
    
    var myEMailVC:MyEMailViewController?
    
    @IBOutlet weak var tabs: UITabBar!
    
    @IBOutlet weak var myAccountTabBar: UITabBar!
    
    var cityFoundInIsrael = true
    
    @IBAction func onNextButtonPress(sender: UIButton) {
        view.endEditing(true)
        print("next button tapped")
        if isPreviousTabsInputCorrect(currentItemTag) {
            switch currentItemTag {
            case 0:
                //            if self.myNameVC?.firstNameTextField?.text != "" && self.myNameVC?.lastNameTextField.text != "" {
                tabs.items![1].enabled = true
                tabs.selectedItem = tabs.items![1]
                firstName = self.myNameVC?.firstNameTextField.text
                lastName = self.myNameVC?.lastNameTextField.text
                switchContainerViewToMyCity()
                
                
            case 1:
                //            if self.myCityVC?.myCityTextField.text != "" && cityFoundInIsrael{
                tabs.items![2].enabled = true
                tabs.selectedItem = tabs.items![2]
                city = self.myCityVC?.myCityTextField.text
                switchContainerViewToMyBirthdayYear()
                
            case 2:
                tabs.items![3].enabled = true
                tabs.selectedItem = tabs.items![3]
                year = myBirthdayYearVC?.pickerYears[myBirthdayYearVC!.yearPickerView.selectedRowInComponent(0)]
                rightNavigationButton.setTitle("Finish", forState: .Normal)
                switchContainerViewToMyEMail()
                
                
            case 3:
                if self.myEMailVC?.myEMailTextField != nil && self.myEMailVC?.myEMailTextField.text != "" {
                    email = self.myEMailVC?.myEMailTextField.text
                }
                let account = createAccount(FirstName: firstName!,LastName: lastName!, City: city!, YearOfBirth: year!, EMail: email)
                tabBarVC?.selectedIndex = 6
                print(account)
                
            default:
                break
            }
        }
    }
    //TODO:- Alert controller
    func isPreviousTabsInputCorrect(itemTag:Int) -> Bool {
        for index in 0...itemTag {
            switch index {
            case 0:
                if self.myNameVC?.firstNameTextField?.text == "" || self.myNameVC?.lastNameTextField.text == "" {
                    tabs.items![1].enabled = false
                    tabs.items![2].enabled = false
                    tabs.items![3].enabled = false
                    return false
                }
            case 1:
                if self.myCityVC?.myCityTextField.text == "" || cityFoundInIsrael == false {
                    tabs.items![2].enabled = false
                    tabs.items![3].enabled = false
                    return false
                }
            case 2:
                continue
            case 3:
                continue
            default:
                return false
            }
        }
        return true
    }
    
    func createAccount(FirstName Firstname:String, LastName:String, City:String, YearOfBirth:Int, EMail:String?) -> Account {
        let account = Account.sharedAccount
        account.firstName = Firstname
        account.lastName = LastName
        account.city = City
        account.year = YearOfBirth
        account.email = EMail
        account.saveAccount()
        return account
    }
    
    
    
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBar.loadJobimColors()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBarVC = self.tabBarController as? MyTabBarControllerViewController
        tabBarVC!.sideMenuVC = self.storyboard?.instantiateViewControllerWithIdentifier("sideMenuVC") as? MenuViewController
        tabBarVC?.sideMenuVC?.tabVC = tabBarVC
        
        print(tabBarVC)
        print(tabBarVC?.sideMenuVC)
        
        tabs.selectedItem = tabs.items![0]
        rightNavigationButton.setTitle("Next", forState: .Normal)
        
        myNameVC?.title = "Name"
        myCityVC?.title = "City"
        myBirthdayYearVC?.title = "Birthday year"
        myEMailVC?.title = "Email"
        
        
        // Do any additional setup after loading the view.
        
        myNameVC = self.childViewControllers[0] as? MyNameViewController
        myCityVC = self.storyboard?.instantiateViewControllerWithIdentifier("myCityVC") as? MyCityViewController
        myBirthdayYearVC = self.storyboard?.instantiateViewControllerWithIdentifier("myBirthdayYearVC") as? MyBirthdayYearViewController
        myEMailVC = self.storyboard?.instantiateViewControllerWithIdentifier("myEMailVC") as? MyEMailViewController
    }
    
    //    var sideMenuVC:MenuViewController?
    
    @IBAction func onMenuButtonPress(sender: UIButton) {
        
        tabBarVC?.sideMenuVC!.showMenu()
    }
    
    func switchContainerViewToMyName() {
        UIView.transitionWithView(self.view, duration: 0.5, options: .TransitionCrossDissolve , animations: { _ in
            
            self.addChildViewController(self.myNameVC!)
            
            self.myNameVC!.view.frame = CGRectMake(0, 0, self.container.frame.size.width, self.container.frame.size.height)
            
            self.container.addSubview(self.myNameVC!.view)
            
            self.myNameVC!.didMoveToParentViewController(self)
            }, completion: nil)
        currentItemTag = 0
        
    }
    
    func switchContainerViewToMyCity() {
        UIView.transitionWithView(self.view, duration: 0.5, options: .TransitionFlipFromRight , animations: { _ in
            
            self.addChildViewController(self.myCityVC!)
            
            self.myCityVC!.view.frame = CGRectMake(0, 0, self.container.frame.size.width, self.container.frame.size.height)
            
            self.container.addSubview(self.myCityVC!.view)
            
            self.myCityVC!.didMoveToParentViewController(self)
            }, completion: nil)
        currentItemTag = 1
    }
    
    func switchContainerViewToMyBirthdayYear() {
        UIView.transitionWithView(self.view, duration: 0.5, options: .TransitionFlipFromRight , animations: { _ in
            
            self.addChildViewController(self.myBirthdayYearVC!)
            
            self.myBirthdayYearVC!.view.frame = CGRectMake(0, 0, self.container.frame.size.width, self.container.frame.size.height)
            
            self.container.addSubview(self.myBirthdayYearVC!.view)
            
            self.myBirthdayYearVC!.didMoveToParentViewController(self)
            }, completion: nil)
        currentItemTag = 2
        
    }
    
    func switchContainerViewToMyEMail() {
        UIView.transitionWithView(self.view, duration: 0.5, options: .TransitionFlipFromRight , animations: { _ in
            
            self.addChildViewController(self.myEMailVC!)
            
            self.myEMailVC!.view.frame = CGRectMake(0, 0, self.container.frame.size.width, self.container.frame.size.height)
            
            self.container.addSubview(self.myEMailVC!.view)
            
            self.myEMailVC!.didMoveToParentViewController(self)
            
            }, completion: nil)
        
        currentItemTag = 3
        
    }
    
    var currentItemTag = 0
    
    func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        switch item.tag {
        case 0:
            print("0")
            if currentItemTag != 0 && isPreviousTabsInputCorrect(0) {
                rightNavigationButton.setTitle("Next", forState: .Normal)
                switchContainerViewToMyName()
            }
        case 1:
            print("1")
            if currentItemTag != 1 && isPreviousTabsInputCorrect(item.tag - 1) {
                rightNavigationButton.setTitle("Next", forState: .Normal)
                switchContainerViewToMyCity()
            }
        case 2:
            print("2")
            if currentItemTag != 2 && isPreviousTabsInputCorrect(item.tag - 1) {
                rightNavigationButton.setTitle("Next", forState: .Normal)
                switchContainerViewToMyBirthdayYear()
            }
        case 3:
            print("3")
            if currentItemTag != 3 && isPreviousTabsInputCorrect(item.tag - 1) {
                rightNavigationButton.setTitle("Finish", forState: .Normal)
                switchContainerViewToMyEMail()
            }
        default:
            break
        }
        tabs.selectedItem = tabs.items![currentItemTag]
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }
    
    
}
