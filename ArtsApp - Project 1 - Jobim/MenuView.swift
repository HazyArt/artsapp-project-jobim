//
//  menuView.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 22 Tamuz 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit

class MenuView: UIView {

    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        
        let path = UIBezierPath()
        let screen = UIScreen.mainScreen().bounds
        path.moveToPoint(CGPoint(x: screen.minX, y: screen.minY))
        path.addLineToPoint(CGPoint(x: screen.minX, y: screen.maxY))
        path.addLineToPoint(CGPoint(x: screen.maxX * 0.8 , y: screen.maxY))
        path.addLineToPoint(CGPoint(x: screen.width * 0.65, y: screen.minY))
        path.closePath()
        UIColor.whiteColor().setFill()
        path.fill()
        
    }
}
