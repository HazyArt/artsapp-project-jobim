//
//  LoggedInMyAccountViewController.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 6 Av 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit

class LoggedInMyAccountViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate{
    
    var imagePicked: UIImage?
    
    @IBOutlet weak var profileImageButton: UIButton!
    
    var imagePicker: UIImagePickerController!
    
    @IBAction func profileImageButtonTapped(sender: AnyObject) {
        self.imagePicker =  UIImagePickerController()
        
        //Create the AlertController
        let actionSheetController: UIAlertController = UIAlertController(title: "Action Sheet", message: "Swiftly Now! Choose an option!", preferredStyle: .ActionSheet)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        actionSheetController.addAction(cancelAction)
        //Create and add first option action
        let takePictureAction: UIAlertAction = UIAlertAction(title: "Take Picture", style: .Default) { action -> Void in
            //Code for launching the camera goes here
            
            self.imagePicker.sourceType = .Camera
            self.presentViewController(self.imagePicker, animated: true, completion: nil)
        }
        actionSheetController.addAction(takePictureAction)
        //Create and add a second option action
        let choosePictureAction: UIAlertAction = UIAlertAction(title: "Choose From Camera Roll", style: .Default) { action -> Void in
            //Code for picking from camera roll goes here
            self.imagePicker.sourceType = .PhotoLibrary
            self.presentViewController(self.imagePicker, animated: true, completion: nil)
        }
        actionSheetController.addAction(choosePictureAction)
        
        //Present the AlertController
        self.presentViewController(actionSheetController, animated: true, completion: nil)
        
        
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        imagePicked = info[UIImagePickerControllerOriginalImage] as? UIImage
    }
    
    
    
    
    
    
    
    var tabBarVC:MyTabBarControllerViewController?
    
    var account:Account?
    
    @IBOutlet weak var menuTableView: UITableView!
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBar.loadJobimColors()
        self.menuTableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        account = Account.sharedAccount
        tabBarVC = self.tabBarController as? MyTabBarControllerViewController
        tabBarVC!.sideMenuVC = self.storyboard?.instantiateViewControllerWithIdentifier("sideMenuVC") as? MenuViewController
        tabBarVC?.sideMenuVC?.tabVC = tabBarVC
        
        menuTableView.backgroundColor = UIColor.clearColor()
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.Plain, target:nil, action:nil)
    }
    
    
    @IBAction func onSettingsButtonPress(sender: UIButton) {
        
    }
    
    @IBAction func onMenuButtonPress(sender: UIButton) {
        
        tabBarVC?.sideMenuVC!.showMenu()
    }
    
    @IBAction func profileImageButtonPressed(sender: UIButton) {
    }
    
    @IBOutlet weak var propertiesTableView: UITableView!
    
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as? LoggedInMyAccountTableViewCell
        switch indexPath.section {
        case 0:
            cell?.iconImage.image = UIImage()
            cell?.cellInfoLabel.text = "\(account!.firstName!) \(account!.lastName!)"
        case 1:
            cell?.iconImage.image = UIImage()
            cell?.cellInfoLabel.text = "\(account!.city!)"
        case 2:
            cell?.iconImage.image = UIImage()
            cell?.cellInfoLabel.text = "\(account!.year!)"
        case 3:
            cell?.iconImage.image = UIImage()
            if account?.email != nil && account?.email != "" {
                
                cell?.cellInfoLabel.text = "\(account!.email!)"
            } else {
                cell?.cellInfoLabel.text = ""
            }
        default:
            break
            
        }
        
        
        
        
        return cell!
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 4
    }
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.clearColor()
        return view
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.section{
        case 0:
            self.performSegueWithIdentifier("nameAndSelfieSegue", sender: self)
        case 1:
            self.performSegueWithIdentifier("citySegue", sender: self)
        case 2:
            self.performSegueWithIdentifier("birthdayYearSegue", sender: self)
        case 3:
            self.performSegueWithIdentifier("emailSegue", sender: self)
        default:
            break
        }
    }
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        let account = Account.sharedAccount
//        switch segue.identifier! {
//        case "nameAndSelfieSegue":
//            let view = segue.destinationViewController as! MyNameViewController
//            view.firstNameTextField.text = account.firstName
//            view.lastNameTextField.text = account.lastName
//        case "citySegue":
//            let view = segue.destinationViewController as! MyCityViewController
//            view.myCityTextField.text = account.city
//        case "birthdayYearSegue":
//            let view = segue.destinationViewController as! MyBirthdayYearViewController
//            let yearIndex = view.pickerYears.indexOf(Account.sharedAccount.year!)
//            view.yearPickerView.selectRow(yearIndex!, inComponent: 0, animated: false)
//        case "var":
//            if let view = segue.destinationViewController as? MyEMailViewController {
//                if account.email != "" && account.email != nil {
//                    view.myEMailTextField.text = account.email
//                }
//            }
//        default:
//            break
//        }
//    }
        
}
