//
//  LeftViewInMainTableViewCell.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 14 Tamuz 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit

class LeftViewInMainTableViewCell: UIView {
    
    @IBOutlet weak var emailButton:UIButton!
    
    @IBOutlet weak var emailLabel:UILabel!
    
    @IBOutlet weak var callButton:UIButton!
    
    @IBOutlet weak var callLabel:UILabel!
    
    @IBOutlet weak var smsButton:UIButton!
    
    @IBOutlet weak var smsLabel:UILabel!
    
    @IBOutlet weak var favoritesButton:UIButton!
    
    @IBOutlet weak var favoritesLabel:UILabel!
    
    var isAddedToFavorites:Bool = false
    // ★☆
    @IBAction func favoritesButtonPressed(sender: UIButton) {
        
        let parent = ((self.superview?.superview?.superview?.superview?.superview?.superview as? UITableView)?.dataSource as? MainViewController)?.parentViewController
        
        let row = (self.superview?.superview?.superview?.superview?.superview?.superview as! UITableView).indexPathForCell(self.superview?.superview?.superview?.superview as! UITableViewCell)?.row
        
        
        print("Favorites pressed")
        isAddedToFavorites = !isAddedToFavorites
        
        let favorites:[Job] = {
            var result:[Job] = []
            for job in Jobs.sharedJobs.list! {
                if job.favorited {
                    result.append(job)
                }
            }
            return result
        }()
        
        if isAddedToFavorites {
            
            switch parent {
            case is MainContainerViewController:
                for job in Jobs.sharedJobs.list! {
                    if job.hashValue == Jobs.sharedJobs.list![row!].hashValue {
                        print("HuliFK")
                        let index = Jobs.sharedJobs.list?.indexOf(job)
                        Jobs.sharedJobs.list![index!].favorited = true
                        Jobs.sharedJobs.saveJobs()
                        break
                    }
                }
            case is MyJobsViewController:
                for job in Jobs.sharedJobs.list! {
                    if job.hashValue == favorites[row!].hashValue {
                        print("HuliFK")
                        let index = Jobs.sharedJobs.list?.indexOf(job)
                        Jobs.sharedJobs.list![index!].favorited = true
                        Jobs.sharedJobs.saveJobs()
                        break
                    }
                }
            default:
                break
            }
            favoritesButton.setImage(UIImage(named: "Star Filled"), forState: .Normal)
            favoritesLabel.text = "Remove from favorites"
            NSNotificationCenter.defaultCenter().postNotificationName("load", object: nil)
            
        } else {
            switch parent {
            case is MainContainerViewController:
                for job in Jobs.sharedJobs.list! {
                    if job.hashValue == Jobs.sharedJobs.list![row!].hashValue {
                        print("HuliFK")
                        let index = Jobs.sharedJobs.list?.indexOf(job)
                        Jobs.sharedJobs.list![index!].favorited = false
                        Jobs.sharedJobs.saveJobs()
                        break
                    }
                }
            case is MyJobsViewController:
                for job in Jobs.sharedJobs.list! {
                    if job.hashValue == favorites[row!].hashValue {
                        print("HuliFK")
                        let index = Jobs.sharedJobs.list?.indexOf(job)
                        Jobs.sharedJobs.list![index!].favorited = false
                        Jobs.sharedJobs.saveJobs()
                        break
                    }
                }
            default:
                break
            }
            favoritesButton.setImage(UIImage(named: "Star Empty"), forState: .Normal)
            favoritesLabel.text = "Add to favorites"
            NSNotificationCenter.defaultCenter().postNotificationName("load", object: nil)
        }
    }
    
    @IBAction func emailButtonPressed(sender:UIButton) {
        print("EMAIL")
    }
    @IBAction func callButtonPressed(sender:UIButton) {
        print("CALL")
    }
    @IBAction func smsButtonPressed(sender:UIButton) {
        print("SMS")
    }
    
    
    /*
     // Only override drawRect: if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func drawRect(rect: CGRect) {
     // Drawing code
     }
     */
    
}
