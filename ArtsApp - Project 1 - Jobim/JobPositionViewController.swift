//
//  JobPositionViewController.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 9 Av 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit

class JobPositionViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var jobsTableView: UITableView!
    
    let professions = [Job.Profession.Bartender,Job.Profession.BranchManager,Job.Profession.Cashier,Job.Profession.ConstructionWorker,Job.Profession.Cook,Job.Profession.Courier,Job.Profession.Driver,Job.Profession.Gardner,Job.Profession.GasStationAttendant,Job.Profession.Host,Job.Profession.Janitor,Job.Profession.KitchenHelper,Job.Profession.Nadlan,Job.Profession.SalesAndServiceRepresentative,Job.Profession.SanitationWorker,Job.Profession.SecuriyGuard,Job.Profession.SellerMan,Job.Profession.ShiftManager,Job.Profession.ShiftManager,Job.Profession.Storekeeper,Job.Profession.Teenages,Job.Profession.Usher,Job.Profession.WaiterWaitress]
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel!.text = professions[indexPath.row].rawValue
        
        
        return cell
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return professions.count
    }

}
