//
//  JobContactOptionsView.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 17 Av 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit

class JobContactOptionsView: UIView {

    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        let path = UIBezierPath()
        let bounds = self.bounds
        path.moveToPoint(CGPoint(x: bounds.minX, y: bounds.minY))
        path.addLineToPoint(CGPoint(x: bounds.minX , y: bounds.maxY))
        path.addLineToPoint(CGPoint(x: bounds.maxX, y: bounds.maxY * 0.8))
        path.addLineToPoint(CGPoint(x: bounds.maxX , y: bounds.maxY * 0.1))
        path.closePath()
        UIColor.magentaColor().setFill()
        path.fill()
        self.clipsToBounds = true
    }
    

}
