//
//  CellViewController.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 14 Av 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit

class CellViewController: UIViewController {
    
    @IBOutlet weak var jobInfoView: JobInfoView!

    @IBOutlet weak var jobContactOptionsView: JobContactOptionsView!
    
    @IBAction func exitButtonTapped(sender: UIButton) {
        print("exit")
        self.dismissViewControllerAnimated(true , completion: nil)
    }
    
    @IBAction func menuButtonTapped(sender: UIButton) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        jobInfoView.backgroundColor = UIColor.init(white: 1, alpha: 0)
        
        jobContactOptionsView.backgroundColor = UIColor.init(white: 1, alpha: 0)

    }

}
