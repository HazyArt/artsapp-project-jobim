//
//  ViewController.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 1 Tamuz 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit
import MessageUI

//MARK:TODO:bezier_path TODO

class ViewController: UIViewController, MFMessageComposeViewControllerDelegate{
    
    let epicOrangeColor = UIColor.init(red: 0.9, green: 0.27, blue: 0, alpha: 1)
    
    @IBOutlet weak var phoneTextField: UITextField!
    
    @IBOutlet weak var il972Label: UILabel!
    @IBOutlet weak var phoneTextFieldBottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        il972Label.layer.borderWidth = 1
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.keyboardWillHide(_:)), name: UIKeyboardWillShowNotification, object: nil)
        // Do any additional setup after loading the view, typically from a nib.
        
         /* TO DO
        let messageController = MFMessageComposeViewController()
        messageController.delegate
        messageController.body
        presentViewController(messageController, animated: true, completion: <#T##(() -> Void)?##(() -> Void)?##() -> Void#>)
 */
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue()) != nil {
//            self.view.frame.origin.y += keyboardSize.height
        }
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
//            self.view.frame.origin.y -= keyboardSize.height
            phoneTextFieldBottomConstraint.constant = keyboardSize.height
        }
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        //MARK:- Reveal Navigation bar
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.phoneTextField.becomeFirstResponder()
        //MARK:- Hide Navigation bar
            self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        //MARK:- Navigation Bar Edit
        navigationController?.navigationBar.barTintColor = epicOrangeColor
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
    }
    //MESSAGE
    // Create a MessageComposer
    
    @IBAction func sendTextMessageButtonTapped(sender: UIButton) {
        let recipient:[String] = ["972"+(phoneTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()))!]
        let messageComposer = MFMessageComposeViewController()
        if MFMessageComposeViewController.canSendText() {
            messageComposer.body = "ArtsApp JOBIM code:1234"
            messageComposer.title = "ArtsApp YO!"
            messageComposer.recipients = recipient
            messageComposer.messageComposeDelegate = self
            self.presentViewController(messageComposer, animated: true, completion: nil)
        } else {
            performSegueWithIdentifier("ViewToSMSVerification", sender: self)
        }
        
    }
    
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        switch result.rawValue{
        case MessageComposeResultCancelled.rawValue:
            print("Message was cancelled")
        case MessageComposeResultFailed.rawValue:
            print("Message failed")
        case MessageComposeResultSent.rawValue:
            print("Message was sent")
        default:
            break
        }
        controller.dismissViewControllerAnimated(false, completion: nil)
        if result.rawValue == MessageComposeResultSent.rawValue {
            performSegueWithIdentifier("ViewToSMSVerification", sender: self)
        }
    }
}

