//
//  LanguageTableViewCell.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 5 Tamuz 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit

class LanguageTableViewCell: UITableViewCell {

    @IBOutlet weak var languageImage: UIImageView!
    
    @IBOutlet weak var languageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
