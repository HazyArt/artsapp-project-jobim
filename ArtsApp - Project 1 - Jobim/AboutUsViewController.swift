//
//  aboutUsViewController.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 4 Av 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController {

    var tabBarVC:MyTabBarControllerViewController?
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBar.loadJobimColors()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarVC = self.tabBarController as? MyTabBarControllerViewController
        tabBarVC!.sideMenuVC = self.storyboard?.instantiateViewControllerWithIdentifier("sideMenuVC") as? MenuViewController
        tabBarVC?.sideMenuVC?.tabVC = tabBarVC
        
        print(tabBarVC)
        print(tabBarVC?.sideMenuVC)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onMenuButtonPress(sender: UIButton) {
        tabBarVC?.sideMenuVC!.showMenu()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
