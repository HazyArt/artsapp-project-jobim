//
//  Job.swift
//  ArtsApp - Project 1 - Jobim
//
//  Created by Hyperactive4 on 7 Tamuz 5776.
//  Copyright © 5776 Hyperactive4. All rights reserved.
//


import UIKit
import Foundation
import CoreLocation

class Job: NSObject, NSCoding {
    override var hashValue: Int {
        return ObjectIdentifier(self).hashValue
    }
    let company:String
    let profession:Profession
    let branch:String?
    let tag:String //Max 50 characters
    let info:String?
    let location:CLLocation
    let photo:UIImage?
    let suitableForTeenagers:Bool
    let question:String?
    let canEmail:Bool
    let emailAdress:String?
    let canCall:Bool
    let callNumber:String?
    let canSms:Bool
    let smsNumber:String?
    var favorited:Bool
    init (company:String, branch:String?,profession:Profession, tag:String, info:String?, location:CLLocation, photo:UIImage?,suitableForTeenagers:Bool ,question:String?, canEmail:Bool, emailAdress:String?,canCall:Bool, callNumber:String?,canSms:Bool,smsNumber:String?) {
        self.company = company
        self.branch = branch
        self.profession = profession
        self.tag = tag
        self.info = info
        self.location = location
        self.photo = photo
        self.suitableForTeenagers = suitableForTeenagers
        self.question = question
        self.canEmail = canEmail
        self.emailAdress = emailAdress
        self.canCall = canCall
        self.callNumber = callNumber
        self.canSms = canSms
        self.smsNumber = smsNumber
        self.favorited = false
    }
    static let allJobs = [Job.Profession.Bartender,Job.Profession.BranchManager,Job.Profession.Cashier,Job.Profession.ConstructionWorker,Job.Profession.Cook,Job.Profession.Courier,Job.Profession.Gardner,Job.Profession.GasStationAttendant,Job.Profession.Host,Job.Profession.Janitor,Job.Profession.KitchenHelper,Job.Profession.Nadlan,Job.Profession.SalesAndServiceRepresentative,Job.Profession.SanitationWorker,Job.Profession.SecuriyGuard,Job.Profession.SellerMan,Job.Profession.ShiftManager,Job.Profession.Storekeeper,Job.Profession.StoreSalesman,Job.Profession.Teenages,Job.Profession.Usher,Job.Profession.WaiterWaitress]
    enum Profession: String {
        case AllJobs = "All Jobs"
        case Bartender = "Bartender"
        case BranchManager = "Branch Manager"
        case Cashier = "Cashier"
        case Cook = "Cook"
        case ConstructionWorker = "Construction Worker"
        case Courier = "Courier"
        case Driver = "Driver"
        case Gardner = "Gardner"
        case GasStationAttendant = "Gas Station Attendant"
        case Host = "Host"
        case Janitor = "Janitor"
        case KitchenHelper = "Kitchen Helper"
        case SalesAndServiceRepresentative = "Sales and Service Representative"
        case SanitationWorker = "Sanitation Worker"
        case SecuriyGuard = "Security Guard"
        case ShiftManager = "Shift Manager"
        case StoreSalesman = "Store Salesman"
        case Storekeeper = "Storekeeper"
        case Teenages = "Teenagers"
        case Usher = "Usher"
        case WaiterWaitress = "Waiter/Waitress"
        case SellerMan = "סוכן מכירות"
        case Nadlan = "נדל״ן"
    }
//    Priority.init(rawValue:) should work.
//    
//    func encodeWithCoder(aCoder: NSCoder) {
//        aCoder.encodeInteger(priority.rawValue, forKey: "priority")
//    }
//    
//    required init(coder aDecoder: NSCoder) {
//        priority = Priority(rawValue: aDecoder.decodeIntegerForKey("priority"))
//    }
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.company, forKey: "jobCompany")
        aCoder.encodeObject(self.branch, forKey: "jobBranch")
        aCoder.encodeObject(self.profession.rawValue, forKey: "jobProfession")
        aCoder.encodeObject(self.tag, forKey:"jobTag")
        aCoder.encodeObject(self.info, forKey: "jobInfo")
        aCoder.encodeObject(self.location, forKey: "jobLocation")
        aCoder.encodeObject(self.photo, forKey: "jobPhoto")
        aCoder.encodeObject(self.suitableForTeenagers, forKey: "jobSuitableForTeenagers")
        aCoder.encodeObject(self.question, forKey: "jobQuestion")
        aCoder.encodeObject(self.canEmail, forKey: "jobCanEmail")
        aCoder.encodeObject(self.emailAdress, forKey: "jobEmailAdress")
        aCoder.encodeObject(self.canSms, forKey: "jobCanSms")
        aCoder.encodeObject(self.smsNumber, forKey: "jobSmsNumber")
        aCoder.encodeObject(self.canCall, forKey: "jobCanCall")
        aCoder.encodeObject(self.callNumber, forKey: "jobCallNumber")
        aCoder.encodeObject(self.favorited, forKey: "jobFavorited")
        
        
    }
    required init?(coder aDecoder: NSCoder) {
        self.company = aDecoder.decodeObjectForKey("jobCompany") as! String
        self.branch = aDecoder.decodeObjectForKey("jobBranch") as? String
        self.profession = Job.Profession(rawValue: aDecoder.decodeObjectForKey("jobProfession") as! String)!
        self.tag = aDecoder.decodeObjectForKey("jobTag") as! String
        self.info = aDecoder.decodeObjectForKey("jobInfo") as? String
        self.location = aDecoder.decodeObjectForKey("jobLocation") as! CLLocation
        self.photo = aDecoder.decodeObjectForKey("jobPhoto") as? UIImage
        self.suitableForTeenagers = aDecoder.decodeObjectForKey("jobSuitableForTeenagers") as! Bool
        self.question = aDecoder.decodeObjectForKey("jobQuestion") as? String
        self.canEmail = aDecoder.decodeObjectForKey("jobCanEmail") as! Bool
        self.emailAdress = aDecoder.decodeObjectForKey("jobEmailAdress") as? String
        self.canCall = aDecoder.decodeObjectForKey("jobCanCall") as! Bool
        self.callNumber = aDecoder.decodeObjectForKey("jobCallNumber") as? String
        self.canSms = aDecoder.decodeObjectForKey("jobCanSms") as! Bool
        self.smsNumber = aDecoder.decodeObjectForKey("jobSmsNumber") as? String
        self.favorited = aDecoder.decodeObjectForKey("jobFavorited") as! Bool
    }
    
    
//
//    func encodeWithCoder(aCoder: NSCoder) {
//
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        
//    }
}

func ==(lhs: Job, rhs: Job) -> Bool {
    return lhs.hashValue == rhs.hashValue
}